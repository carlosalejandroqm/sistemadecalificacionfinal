
package world;

/**
 *
 * @author carlos
 */
import util.*; 
import util_archivo.*; 

public class SistemaCalificacion {
    Cola<Materia> materias = new Cola(); 
    ListaCD<Estudiante>estudiantes = new ListaCD(); 
    
    
    public SistemaCalificacion(String urlMaterias , String urlEstudiantes , String urlEnsayos  ){
        
        this.crearMateria(urlMaterias);
        this.crearEstudiante(urlEstudiantes);
        this.crearEnsayo(urlEnsayos);
        
    }
    
    
    public Materia getMateria(byte codMateria){
        Cola<Materia> aux = new Cola(); 
        Materia m = new Materia(); 
        m.setCodMateria(codMateria);
        int contador = materias.getTamanio() ; 
        while(m.getCodMateria() != materias.getInfoInicio().getCodMateria()){
            if(contador == 0 ){
                return null ; 
            }
            aux.enColar(materias.getInfoInicio());
            materias.deColar(); 
               
            contador--; 
        }
        m = materias.getInfoInicio(); 
        materias = aux ; 
        
        return m ; 
        
    }
    
    public void crearMateria(String urlMateria ){
        ArchivoLeerURL file = new ArchivoLeerURL(urlMateria); 
        Object []v = file.leerArchivo(); 
        
        for (int i = 1; i < v.length; i++) {
            String fila = v[i].toString(); 
            String []datos = fila.split(";"); 
            
            byte codMateria = Byte.parseByte(datos[0]); 
            String nombre = datos[1];
            
            Materia m = new Materia ( codMateria , nombre ); 
            
            this.materias.enColar(m);
            
        }
        
        
    }
    
    
    
    private Estudiante getEstudiante(int codigoEstudiante) {
        for (Estudiante e : estudiantes) {
            if (e.getCodigo() == codigoEstudiante) {
                return e;
            }
        }
        return null;
    }
    
    public void crearEstudiante(String urlEstudiante){
         ArchivoLeerURL file = new ArchivoLeerURL(urlEstudiante); 
        Object []v = file.leerArchivo(); 
        
        for (int i = 1; i < v.length; i++) {
            String fila = v[i].toString(); 
            String []datos = fila.split(";"); 
            
            int codigo = Integer.parseInt(datos[0]);
            String nombre = datos[1]; 
            boolean genero = Boolean.parseBoolean(datos[2]); 
            
            Estudiante e = new Estudiante(codigo , nombre , genero ); 
            this.estudiantes.insertarAlInicio(e);
        }
        
        
    }
    
    public void crearEnsayo(String urlEnsayo){
        ArchivoLeerURL file = new ArchivoLeerURL(urlEnsayo); 
        Object []v = file.leerArchivo(); 
        
        for (int i = 1; i < v.length; i++) {
            
            String fila = v[i].toString(); 
            String []datos = fila.split(";"); 
            
            byte codMateria = Byte.parseByte(datos[0]); 
            int codEst = Integer.parseInt(datos[1]);
            String cuerpoTrabajo = datos[2];
            int cantHojas = Integer.parseInt(datos[3]); 
            
            
            Materia m = this.getMateria(codMateria); 
            Estudiante est = this.getEstudiante(codEst);
            
            
            Ensayo e = new Ensayo(codMateria ,  codEst , cuerpoTrabajo , cantHojas); 
            
        }
        
        
    }
    
    
    public String calcularReporte(){
        String reporte  =""; 
        
        return reporte ; 
        
    }
    
    
}
