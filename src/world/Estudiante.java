
package world;

/**
 *
 * @author carlos
 */
public class Estudiante {
    
    // propiedades 
    
    private int codigo ; 
    private String nombre ; 
    private boolean genero ; 

    /**constructor por defecto */
    public Estudiante() {
    }
    
    /**constructor con parametros */
    public Estudiante(int codigo, String nombre, boolean genero) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.genero = genero;
    }
    
    //START GETTER AND SETTER METHODS 

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean getGenero() {
        return genero;
    }

    public void setGenero(boolean genero) {
        this.genero = genero;
    } 
    
    /**metodo toString*/
    
    @Override
    public String toString() {
        return "Estudiante{" + "codigo=" + codigo + ", nombre=" + nombre + ", genero=" + genero + '}';
    }
    
    
    
    
   
    
}
