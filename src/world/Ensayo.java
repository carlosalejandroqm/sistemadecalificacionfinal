package world;

/**
 *
 * @author carlos
 */
public class Ensayo {
    // propiedades 

    private String contenidoEnsayo;
    private int cantidadHojas;
    private Estudiante estudiante;

    /**
     * constructor por defecto
     */
    public Ensayo() {
    }

    /**
     * constructor con parametros
     */
    public Ensayo(String contenidoEnsayo, int cantidadHojas, Estudiante estudiante) {
        this.contenidoEnsayo = contenidoEnsayo;
        this.cantidadHojas = cantidadHojas;
        this.estudiante = estudiante;
    }

    /**
     * metodo que calcula la nota de cada ensayo
     */
    public Double getNota() {
        double nota = 0;

        if (estudiante.getGenero()) {
            if (cantidadHojas < 3) {
                nota = 3.0;

            } else if (cantidadHojas >= 3 && cantidadHojas <= 5) {
                nota = 4.0;
            } else {
                nota = 5.0;
            }

        } else {
            if (cantidadHojas < 3) {
                nota = 2.0;

            } else if (cantidadHojas >= 3 && cantidadHojas <= 5) {
                nota = 3.0;
            } else {
                nota = 4.0;
            }

        }
        return nota ; 
    }

    //START GETTER AND SETTER METHODS 
    public String getContenidoEnsayo() {
        return contenidoEnsayo;
    }

    public void setContenidoEnsayo(String contenidoEnsayo) {
        this.contenidoEnsayo = contenidoEnsayo;
    }

    public int getCantidadHojas() {
        return cantidadHojas;
    }

    public void setCantidadHojas(int cantidadHojas) {
        this.cantidadHojas = cantidadHojas;
    }

    public Estudiante getEstudiante() {
        return estudiante;
    }

    public void setEstudiante(Estudiante estudiante) {
        this.estudiante = estudiante;
    }

    /**
     * metodo tostring
     */
    @Override
    public String toString() {
        return "Ensayo{" + "contenidoEnsayo=" + contenidoEnsayo + ", cantidadHojas=" + cantidadHojas + ", estudiante=" + estudiante + '}';
    }

}
