package world;

/**
 *
 * @author carlos
 */
import util.*;

public class Materia {
    //propiedades 

    private byte codMateria;
    private String nombreMateria;
    private Pila<Ensayo> ensayos = new Pila();

    /**
     * constructor por defecto
     */
    public Materia() {
    }

    /**
     * constructor con parametros
     */
    public Materia(byte codMateria, String nombreMateria) {
        this.codMateria = codMateria;
        this.nombreMateria = nombreMateria;
    }

    // metodo decorador , encapsula la funcion apilar o añadir 
    public void agregarEnsayo(Ensayo e) {
        this.ensayos.apilar(e);
    }

    // se intenta realizar este metodo mas modularmente , falta revisarlo 
    public ListaCD<Double> BuscarEStudianteEnEnsayo(Estudiante e) {
        Pila<Ensayo> aux = new Pila<>();
        ListaCD<Double> notas = new ListaCD<>();

        while (!ensayos.esVacia()) {

            Ensayo ensayoAux = ensayos.desapilar();

            if (ensayoAux.getEstudiante().equals(e)) {
                notas.insertarOrdenado(ensayoAux.getNota());
            } else {
                aux.apilar(ensayoAux);
            }
        }
        ensayos = aux;

        return notas;
    }

    public int cantidadDeEnsayos(Estudiante e) {
        ListaCD<Double> notas = this.BuscarEStudianteEnEnsayo(e);
        int cantidadNotas = notas.getTamanio();
        int acum = 0;

        if (notas.getTamanio() > 0) {

            if (cantidadNotas <= 2) {
                for (Double i : notas) {
                    acum += i;
                }

            } else {
                // si tiene mas de dos ensayos no va a considerar los dos primeros 
                for (int i = 2; i < notas.getTamanio(); i++) {
                    acum += notas.get(i);
                }
            }

        }
        if (cantidadNotas > 2) {
            cantidadNotas -= 2;

        }
        return acum / cantidadNotas;

    }

    //START GETTER AND SETTER METHODS 
    public byte getCodMateria() {
        return codMateria;
    }

    public void setCodMateria(byte codMateria) {
        this.codMateria = codMateria;
    }

    public String getNombreMateria() {
        return nombreMateria;
    }

    public void setNombreMateria(String nombreMateria) {
        this.nombreMateria = nombreMateria;
    }

    public Pila<Ensayo> getEnsayos() {
        return ensayos;
    }

    public void setEnsayos(Pila<Ensayo> ensayos) {
        this.ensayos = ensayos;
    }

    /**
     * Metodo toString
     */
    @Override
    public String toString() {
        return "Materia{" + "codMateria=" + codMateria + ", nombreMateria=" + nombreMateria + ", ensayos=" + ensayos + '}';
    }

}
